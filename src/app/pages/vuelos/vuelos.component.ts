import { Component, OnInit } from '@angular/core';
import { VuelosSerService } from 'src/app/services/vuelos-ser.service';

@Component({
  selector: 'app-vuelos',
  templateUrl: './vuelos.component.html',
  styleUrls: ['./vuelos.component.scss']
})
export class VuelosComponent implements OnInit {

  datare:any;
  itinerary:any[]=[];
  fligthSegment:any[]=[];
  constructor(private DataServ:VuelosSerService) { }

  ngOnInit() {

    this.DataServ.getData()
    .subscribe((data:Object) => {
      this.datare = data;
      console.log(data)
      this.itinerary = this.datare.result.OTA_AirLowFareSearchRS.PricedItineraries.PricedItinerary;
      console.log(this.itinerary.length);
      let arreglo:any[]=[];
      for (let i = this.itinerary.length - 1; i >= 0; i--) {
        //console.log(this.itinerary[i].AirItinerary.OriginDestinationOptions.OriginDestinationOption[0].FlightSegment);
        arreglo.push(this.itinerary[i].AirItinerary.OriginDestinationOptions.OriginDestinationOption[0].FlightSegment);
      }
      this.fligthSegment = arreglo
      console.log(this.fligthSegment);
    });
    console.log("fin")
  }
}


