import { TestBed } from '@angular/core/testing';

import { VuelosSerService } from './vuelos-ser.service';

describe('VuelosSerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VuelosSerService = TestBed.get(VuelosSerService);
    expect(service).toBeTruthy();
  });
});
