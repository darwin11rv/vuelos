import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VuelosSerService {

  url = "https://miagenciamt.com/api/public/api/search?origin=UIO&destination=GYE&departureDate=2019-05-21&arrivalDate=2019-05-23&numAdultos=1&numCnn=&numInf=&clase=Y";

  constructor( private http: HttpClient) { }


  getData(){
    return this.http.get(this.url);
  }

}