import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VuelosComponent } from './pages/vuelos/vuelos.component';

const routes: Routes = [
  {
    path:"vuelos",
    component:VuelosComponent
  },
  {
    path:"**",
    redirectTo:'vuelos'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
